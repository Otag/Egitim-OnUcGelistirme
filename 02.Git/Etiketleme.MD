# İmleme

Katkılara im eklemeniz katkı geçmişinizdeki önemli noktaları daha net görebilmeniz açısından önemlidir.

Bunun için `git tag` komutu vardır.

## Katkı imi oluşturma

```bash
  git tag 1.0s
```

Dilerseniz ileti de iliştirebilirsiniz

```bash
  git tag -a 1.0s -m "1.0 sürümü"
```

## İm ayrıntıları

İm ayrıntıları katkı ayrıntılarıyla benzerlik gösterir.

```bash
  git show 1.0s
```

## Sonradan imleme

Eğer geçmişteki bir katkıya im eklemek isterseniz katkı mührünü yazmalısınız

```bash
  git tag -a 1.1 9dfefaa
```

## İm silme

İmlemekten vazgeçerseniz şu komutla kaldırabilirsiniz

```bash
  git tag -d 1.1
```

## İmleri yollama

`git push` komutu ön tanımlı olarak imleri yollamayacak biçimdedir.

Eklediğiniz imleri uzak git kaynağına yollamak için `git push --tags` komutunu kullanmalısınız.

```bash
  git push origin --tags
```

## Geri dönme

Önceki imlerden birine geri dönmek isterseniz katkı mührü yazmanıza gerek yoktur. `git checkout` komutu sürümleri de tanıyacak biçimde çalışır.

```bash
  git checkout 1.0s
```

Ancak bu biçimde bir kullanım, başı, çıkarılmış duruma sokar `detached HEAD state`. Başsız devlet ayakta durmaz.

İlgili konuya "git kullanırken özen gösterilmesi gereken konular" başlığı altında değinilecektir.