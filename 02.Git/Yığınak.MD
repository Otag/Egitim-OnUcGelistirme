# Yığınak Oluşturma

## Yerelde yığınak oluşturma

Komut satırınıza aşağıdaki komutları yazarak bir dizin oluşturun ve git'i bu dizine ekleyin

```bash
mkdir YIGINAK_ADI
cd YIGINAK_ADI

git init
```

**CTRL + X** ile çıkış yaparken **Y** ye basıp kaydedin

## GitLab Arayüzünde Tasarı Oluşturma


[GitLab Arayüzü](https://gitlab.com/projects/new) üzerinden yeni tasarı oluşturun.

Tasarının gizlilik düzeyini seçin.

## Yerel ile Uzak Yığınağı bağlama

Bulunduğunuz dizinde .git/config adlı ayar belgesini düzenleyin. Oluşturduğunuz SSH Açarınızın ve Yığınağınızın yolunu belirtin.

```bash
nano .git/config
```

```
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
        ignorecase = true
        precomposeunicode = true
[remote "origin"]
        url = https://gitlab.com/KULLANICI/YIGINAK_ADI.git
        fetch = +refs/heads/*:refs/remotes/origin/*
        IdentityFile = ~/.ssh/ACAR_ADI
[branch "master"]
        remote = origin
        merge = refs/heads/master
```

**CTRL + X** ile çıkış yaparken **Y** ye basıp kaydedin
