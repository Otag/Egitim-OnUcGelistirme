let Kabuk = 'Kabuk'.has({
  Yön: 'Yönlendirme',
  Bet: 'Bet'
})
let Sözlük 
let routes = {
  index: 'ana',
  ana: 'Ana Bet'.has({
    name:'Ana',
    açıklama:''.set('Bu bir sözlük uygulamasıdır')
  }),
  sözlük: 'Sözlük'.prop({
    name:'Sözlük',
    once(){
      let sözlük = O.Disk.sözlük
      if(sözlük){
        return this.kur(Sözlük=Dizi(sözlük))
      }
      O.req('./sozluk.json').then(sözlük=>this.kur(Sözlük=Dizi(O.Disk.sözlük=sözlük)))
    },
    kur(sözlük){
      this.html(
        Object.keys(sözlük)
          .filter(sözcük=> sözlük[sözcük] instanceof Object)
          .map(O.Model.DizelgedekiSözcük)
      )
    }
  }),
  sözcük: 'Sözcük'
  .has({
    yabancı:'yabancı'.has({
      dil:O.Model.Dil,
      sözcük:O.Model.Sözcük
    })
  })
  .prop({
    name:'Sözcük',
    diller:['en','ar','fr','it'],
    once(){
      //bayrakları as
    },
    wake(sözcük){
      if(!sözcük){
        O.Page.routeSilent('sözcük','-denkgele')
      }
      if(sözcük=='-denkgele'){
        O.Page.routeSilent('sözcük',Object.keys(O.Disk.sözlük))
      }
      let söz=O.Disk.sözlük[sözcük]
      if(söz instanceof String){
        O.Page.routeSilent('sözcük',söz)
      }else{
        let dil=Object.keys(söz).reduce((dil,açar)=>{
          if(this.diller.indexOf(açar)>-1){
            dil=açar
          }
          return dil
        },'')
        this.V('yabancı').value={
          dil,
          sözcük:söz[dil]
        }
      }
    }
  })
}

O.Page({routes,handler:Kabuk.V('Bet'),Nav:Kabuk.V('Yön')})
O.ready.then(b=>b.html(Kabuk))